#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>
#include "bzrc.h"
#include "dumb.h"

#define IP_ADDRESS "127.0.0.1"
#define SIXTY_DEG M_PI/3
#define TOMICRO(x) x*1000000

extern int debug;

int main(int argc, char* argv[]) {
	int tcpSocket = 0;
	int tankCount;
	int i;
	FILE* fSocket = 0;
	pthread_t* agents;
	param* params;
	tank* tanks;
	
	srand(time(NULL));	

	if (argc < 2) {
		fprintf(stderr, "usage: bzrc port [debug 1:0]\n");
		return -1;
	}
	if (argc == 3 && atoi(argv[2]) == 1) {
		debug = 1;
	}
	
	if(BZRCConnect(IP_ADDRESS, atoi(argv[1]), &tcpSocket, &fSocket)) return -1;

	if(getMyTanks(fSocket, &tanks, &tankCount)) {
		BZRCDisconnect(tcpSocket, fSocket);
		return -1;
	}
	agents = (pthread_t*)malloc(sizeof(pthread_t) * tankCount);
	params = (param*)malloc(sizeof(params) * tankCount);
	for(i = 0; i < tankCount; i++) {
		params[i].tankID = tanks[i].index;
		params[i].fSocket = fSocket;
		pthread_create(&agents[i], NULL, &runAgent, (void*)&params[i]);
	}
	for(i = 0; i < tankCount; i++) {
		pthread_join(agents[i], NULL);
	}
	free(params);
	free(agents);
	free(tanks);
	BZRCDisconnect(tcpSocket, fSocket);

	return 0;
}

float normalizeAngle(float angle) {
	angle -= 2 * M_PI * (int)(angle/(2*M_PI));
	if (angle <= -M_PI)
	{
		angle += 2 * M_PI;
	}
	else if (angle > M_PI)
	{
		angle -= 2 * M_PI;
	}
	return angle;
}

void *dumbShoot(void *agent_param)
{
	param *agent = (param*) agent_param;
	int sleep_time;
	while(1)
	{
		shoot(agent->fSocket, agent->tankID);
		sleep_time = (rand() % 1000000) + 1500000;
		usleep(sleep_time);
	}
}

void *runAgent(void *agent_param)
{
	param *agent = (param*) agent_param;
	int move_forward = 0;
	int tankCount;
	int sleep_time;
	float cur_angle, target_angle, relative_angle;
	time_t start = time(NULL);
	tank* tanks;
	pthread_t gun;
	pthread_create(&gun, NULL, &dumbShoot, agent);

	while(1)
	{
		setSpeed(agent->fSocket, agent->tankID, 0);

		getMyTanks(agent->fSocket, &tanks, &tankCount);
		cur_angle = tanks[agent->tankID].angle;
		free(tanks);
		target_angle = cur_angle + SIXTY_DEG;
		relative_angle = normalizeAngle(target_angle - cur_angle);
		while(fabs(relative_angle) > 0.005)
		{
			getMyTanks(agent->fSocket, &tanks, &tankCount);
			cur_angle = tanks[agent->tankID].angle;
			relative_angle = normalizeAngle(target_angle - cur_angle);
			setAngVel(agent->fSocket, agent->tankID, relative_angle);
		}
		setAngVel(agent->fSocket, agent->tankID, 0);
		// go forward
		if(setSpeed(agent->fSocket, agent->tankID, 1)) return;
		sleep_time = (rand() % 6000000) + 3000000;
		usleep(sleep_time);
	}
	pthread_join(gun, NULL);
}
