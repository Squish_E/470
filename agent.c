#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "bzrc.h"
#include "agent.h"

#define IP_ADDRESS "127.0.0.1"
#define WORLD_UPDATE_RATE 1000
#define SIXTY_DEG M_PI/3
#define TOMICRO(x) x*1000000

#define ALPHA 0.7f
#define BETA  0.65f
#define TAN_BETA 0.001f

#define MY_INFINITY 50

#define GOAL_INFLUENCE 40.0f
#define OBJ_INFLUENCE 60.0f
#define TAN_INFLUENCE 0.1f

extern int debug;

sem_t world_init_mutex;
sem_t update_mutex;

int running = 1;

int main(int argc, char* argv[]) {
	int tcpSocket, i = 0;
	world_info world;
	pthread_t* agents;
	pthread_t update_thread;
	param* params;
	constant* constants;
	int constantCount;
	char* teamColor;

	// Initialize
	sem_init(&world_init_mutex, 0, 0);
	sem_init(&update_mutex, 0, 1);
	srand(time(NULL));	

	if (argc < 2) {
		fprintf(stderr, "usage: bzrc port [debug 1:0]\n");
		return -1;
	}
	if (argc == 3 && atoi(argv[2]) == 1) {
		debug = 1;
	}
	
	BZRCConnect(IP_ADDRESS, atoi(argv[1]), &tcpSocket, &world.fSocket);
	pthread_create(&update_thread, NULL, &updater, (void*)&world);
	
	getConstants(world.fSocket, &constants, &constantCount);
	for(i = 0; i < constantCount; i++) {
		if(!strncmp("team", constants[i].name, 4)) {
			teamColor = constants[i].value;
			break;
		}
	}

	sem_wait(&world_init_mutex);
	//world.tankCount = 1;
	agents = (pthread_t*)malloc(sizeof(pthread_t) * world.tankCount);
	params = (param*)malloc(sizeof(params) * world.tankCount);
	for(i = 0; i < world.tankCount; i++) {
		params[i].tankID = world.tanks[i].index;
		params[i].teamColor = teamColor;
		params[i].world = &world;
		pthread_create(&agents[i], NULL, &runAgent, (void*)&params[i]);
	}
	for(i = 0; i < world.tankCount; i++) {
		pthread_join(agents[i], NULL);
	}
	free(params);
	free(agents);
	free(constants);
	free(world.flags);
	free(world.tanks);
	free(world.bases);
	free(world.obstacles);
	sem_destroy(&world_init_mutex);
	sem_destroy(&update_mutex);
	BZRCDisconnect(tcpSocket, world.fSocket);

	return 0;
}

void *updater(void* args) {
	FILE* fSocket;
	world_info* world = (world_info*)args;
	fSocket = world->fSocket;
	getFlags(fSocket, &world->flags, &world->flagCount);
	getMyTanks(fSocket, &world->tanks, &world->tankCount);
	getBases(fSocket, &world->bases, &world->baseCount); // bases are static, don't update
	getObstacles(fSocket, &world->obstacles, &world->obstacleCount);
	sem_post(&world_init_mutex);
	while (running) {
		usleep(WORLD_UPDATE_RATE);
		sem_wait(&update_mutex);
			free(world->flags);
			getFlags(fSocket, &world->flags, &world->flagCount);
			free(world->tanks);
			getMyTanks(fSocket, &world->tanks, &world->tankCount);
		sem_post(&update_mutex);
	}
	return 0;
}

float normalizeAngle(float angle) {
	angle -= 2 * M_PI * (int)(angle/(2*M_PI));
	if (angle <= -M_PI) {
		angle += 2 * M_PI;
	}
	else if (angle > M_PI) {
		angle -= 2 * M_PI;
	}
	return angle;
}
int getGoal(int* retID, pair* retGoal, param* agent, tank curTank) {
	base curBase;
	flag curFlag;
	float minDis = 10000; // XXX Max distance
	float tmpDis;
	int minID = -1;
	int i;
	// If we have a flag go home
	if(strncmp("-", curTank.flag, 1)) {
		for(i = 0; i < agent->world->baseCount; i++) {
			curBase = agent->world->bases[i];
			// Our base
			if(!strncmp(agent->teamColor, curBase.color, 16)) {
				*retGoal = centerB(curBase);
				*retID = -1;
				return 0;
			}
		}
	}
	else {
		for(i = 0; i < agent->world->flagCount; i++) {
			curFlag = agent->world->flags[i];
			// Our Flag
			if(!strncmp(agent->teamColor, curFlag.ownerColor, 16)) {
				// If someone is holding it
				if(strncmp("none", curFlag.holderColor, 16)) {
					tmpDis = getDisP(curTank.location, curFlag.location);
					if(tmpDis < minDis) {
						minDis = tmpDis;
						minID = i;
					}
				}
			}
			// Not our flag
			else {
				if(strncmp(agent->teamColor, curFlag.holderColor, 16)) {
					tmpDis = getDisP(curTank.location, curFlag.location);
					if(tmpDis < minDis) {
						minDis = tmpDis;
						minID = i;
					}
				}
			}
		}
		if(minID > -1){
			if(strncmp(agent->teamColor, agent->world->flags[minID].holderColor, 16)) {
				if(strncmp("none", agent->world->flags[minID].holderColor, 16)) {
					shoot(agent->world->fSocket, agent->tankID);
				}
			}
		}
		//if(minID > -1) { printf("%s going to %s\n", agent->teamColor, agent->world->flags[minID].ownerColor);}
		*retID = minID;
		//No Flags open
		if(minID == -1) {
			retGoal->x = curTank.location.x;
			retGoal->y = curTank.location.y;
			return -1;
		}
		//Ret Flag
		else {
			retGoal->x = agent->world->flags[minID].location.x;
			retGoal->y = agent->world->flags[minID].location.y;
			return 0;
		}
	}
}

int timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1) {
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;

    return (diff<0);
}

pair centerB(base obj) {
	pair toRet;
	toRet.x = (obj.corners[0].x + obj.corners[1].x + obj.corners[2].x + obj.corners[3].x) / 4;
	toRet.y = (obj.corners[0].y + obj.corners[1].y + obj.corners[2].y + obj.corners[3].y) / 4;
	return toRet;
}

pair center(obstacle obj) {
	pair toRet;
	toRet.x = (obj.corners[0].x + obj.corners[1].x + obj.corners[2].x + obj.corners[3].x) / 4;
	toRet.y = (obj.corners[0].y + obj.corners[1].y + obj.corners[2].y + obj.corners[3].y) / 4;
	return toRet;
}

double square(double toSquare) {
	return toSquare * toSquare;
}

double getDis(double one, double two) {
	return sqrt(square(one) + square(two));
}

double getDisP(pair one, pair two) {
	return getDis(one.x - two.x, two.y - one.y);
}

double getAngle(double one, double two) {
	return atan2(one, two);
}

double getAngleP(pair pointOne, pair pointTwo) {
	return getAngle(pointOne.y - pointTwo.y, pointOne.x - pointTwo.x);
}

pair attractField(pair cur_pos, pair goal_pos, double radius) {
	double dist = getDisP(goal_pos, cur_pos);
	double angle = getAngleP(goal_pos, cur_pos);
	pair delta;
	if(dist < radius) {
		delta.x = 0;
		delta.y = 0;
	}
	else if(radius <= dist && dist <= (GOAL_INFLUENCE + radius)) {
		delta.x = ALPHA * (dist - radius) * cos(angle);
		delta.y = ALPHA * (dist - radius) * sin(angle);
	}
	else if (dist > (radius + GOAL_INFLUENCE)) {
		delta.x = ALPHA * GOAL_INFLUENCE * cos(angle);
		delta.y = ALPHA * GOAL_INFLUENCE * sin(angle);
	}
	return delta;
	
}

pair reflectField(pair cur_pos, pair obj_pos, double radius) {
	double dist = getDisP(obj_pos, cur_pos);
	double angle = getAngleP(obj_pos, cur_pos);
	pair delta;
	if (dist < radius) {
		delta.x = -cos(angle) * MY_INFINITY;
		delta.y = -sin(angle) * MY_INFINITY;
	}
	else if (radius <= dist && dist <= (OBJ_INFLUENCE + radius)) {
		delta.x = -BETA * (OBJ_INFLUENCE + radius - dist) * cos(angle);
		delta.y = -BETA * (OBJ_INFLUENCE + radius - dist) * sin(angle);
	}
	else if (dist > (OBJ_INFLUENCE + radius)) {
		delta.x = 0;
		delta.y = 0;
	}
	return delta;
}

pair tanField(pair cur_pos, pair obj_pos, double radius) {
	double dist = getDisP(obj_pos, cur_pos);
	double angle = getAngleP(obj_pos, cur_pos) + M_PI/2;
	pair delta;
	if (dist < radius) {
		delta.x = -cos(angle) * MY_INFINITY;
		delta.y = -sin(angle) * MY_INFINITY;
	}
	else if (radius <= dist && dist <= (OBJ_INFLUENCE + radius)) {
		delta.x = -TAN_BETA * (TAN_INFLUENCE + radius - dist) * cos(angle);
		delta.y = -TAN_BETA * (TAN_INFLUENCE + radius - dist) * sin(angle);
	}
	else if (dist > (TAN_INFLUENCE + radius)) {
		delta.x = 0;
		delta.y = 0;
	}
	return delta;
}

double moveAngle(double dx, double dy, double cur_angle, double prev_angle, float timeDiff) {
	double goal_angle, error_angle, true_angle;
	goal_angle = atan2(dy, dx);
	cur_angle = normalizeAngle(cur_angle);
	error_angle = goal_angle - cur_angle;
	true_angle = error_angle + ((error_angle - prev_angle) / timeDiff);
	if (true_angle > M_PI) {
		true_angle = true_angle - 2 * M_PI;
	}
	else if (true_angle < -M_PI) {
		true_angle = 2 * M_PI + true_angle;
	}
	return true_angle;
}


void *runAgent(void *agent_param) {
	param *agent = (param*) agent_param;
	tank curTank;
	pair goal;
	int i, j;
	// potential fields
	pair cur_delta;
	obstacle curObj;
	double r, dx, dy, speed;
	// PD controller
	double prev_angle = 0;
	agent->flagGoalID = -1;
	struct timeval tvBegin, tvEnd, tvDiff;

	while(running) {
		gettimeofday(&tvBegin, NULL);
		usleep(WORLD_UPDATE_RATE); //wait for a tick
		sem_wait(&update_mutex);
		curTank = agent->world->tanks[agent->tankID];
		getGoal(&agent->flagGoalID, &goal, agent, curTank);
		sem_post(&update_mutex);
		gettimeofday(&tvEnd, NULL);
		timeval_subtract(&tvDiff, &tvEnd, &tvBegin);

		dx = dy = 0;
		// Attractive Field toward Goal
		cur_delta = attractField(curTank.location, goal, 3.0f);
		dx += cur_delta.x;
		dy += cur_delta.y;
		for(i = 0; i < agent->world->obstacleCount; i++) {
			curObj = agent->world->obstacles[i];
			r = (((curObj.corners[0].x - 
				curObj.corners[3].x) + 
				(curObj.corners[0].y - 
				curObj.corners[1].y)) /2) * .7;
			// Reflective Field
			cur_delta = reflectField(curTank.location, center(curObj), r);
			dx += cur_delta.x;
			dy += cur_delta.y;
			//Tangental
			cur_delta = tanField(curTank.location, center(curObj), r);
			dx += cur_delta.x;
			dy += cur_delta.y;
		}
		// PD Controller
		prev_angle = moveAngle(dx, dy, curTank.angle, prev_angle, (float) tvDiff.tv_usec);
		speed = 1.2f * (-pow((fabs(prev_angle / M_PI)), (float)1.0f/4.0f) +1);
		// If we aren't aimed in the right direction don't move
		if(fabs(prev_angle) > M_PI - (M_PI/4)) {
			speed = -speed;
		} 
		else if(fabs(prev_angle) > M_PI / 4) {
			speed = 0;
		}
		setSpeed(agent->world->fSocket, agent->tankID, speed);
		setAngVel(agent->world->fSocket, agent->tankID, prev_angle / M_PI);
	}
}
