#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <math.h>
#include "bzrc.h"
#include "dumb.h"

#define IP_ADDRESS "127.0.0.1"
#define SIXTY_DEG M_PI/3
#define TOMICRO(x) x*1000000

extern int debug;

void *nothing(void *agent_param) {
	param *agent = (param*) agent_param;
	setSpeed(agent->fSocket, agent->tankID, 0);
	setAngVel(agent->fSocket, agent->tankID, 0);
}

void *straight(void *agent_param) {
	param *agent = (param*) agent_param;
	setSpeed(agent->fSocket, agent->tankID, 1);
	setAngVel(agent->fSocket, agent->tankID, 0);
}

void *violateKalman(void *agent_param) {
	param *agent = (param*) agent_param;
	setSpeed(agent->fSocket, agent->tankID, 1);
	setAngVel(agent->fSocket, agent->tankID, 1);
}

int main(int argc, char* argv[]) {
	int tcpSocket = 0;
	int tankCount;
	int i;
	FILE* fSocket = 0;
	pthread_t* agents;
	param* params;
	tank* tanks;
	
	srand(time(NULL));	

	if (argc < 2) {
		fprintf(stderr, "usage: bzrc port [debug 1:0]\n");
		return -1;
	}
	if (argc == 3 && atoi(argv[2]) == 1) {
		debug = 1;
	}
	
	if(BZRCConnect(IP_ADDRESS, atoi(argv[1]), &tcpSocket, &fSocket)) return -1;

	if(getMyTanks(fSocket, &tanks, &tankCount)) {
		BZRCDisconnect(tcpSocket, fSocket);
		return -1;
	}
	agents = (pthread_t*)malloc(sizeof(pthread_t) * tankCount);
	params = (param*)malloc(sizeof(params) * tankCount);
	for(i = 0; i < tankCount; i++) {
		params[i].tankID = tanks[i].index;
		params[i].fSocket = fSocket;
		pthread_create(&agents[i], NULL, &straight, (void*)&params[i]);
	}
	for(i = 0; i < tankCount; i++) {
		pthread_join(agents[i], NULL);
	}
	free(params);
	free(agents);
	free(tanks);
	BZRCDisconnect(tcpSocket, fSocket);

	return 0;
}
