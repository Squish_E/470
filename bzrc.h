#ifndef BZRC_H
#define BZRC_H

typedef struct team {
	char color[16];
	int playerCount;
} team;

typedef struct pair {
	float x;
	float y;
} pair;

typedef struct obstacle {
	pair corners[4];
} obstacle;

typedef struct base {
	char color[16];
	pair corners[4];
} base;

typedef struct flag {
	char ownerColor[16];
	char holderColor[16];
	pair location;
} flag;

typedef struct shot {
	pair location;
	pair velocity;
} shot;

typedef struct tank {
	int index;
	char callsign[16];
	char status[16];
	int shotsAvailble;
	float reloadTime;
	char flag[16];
	pair location;
	float angle;
	pair velocity;
	float angVel;
} tank;

typedef struct enemyTank {
	char callsign[16];
	char color[16];
	char status[16];
	char flag[16];
	pair location;
	float angle;
} enemyTank;

typedef struct constant {
	char name[16];
	char value[32];
} constant;

typedef struct occgrid {
	pair topleft;
	pair size;
	char* grid;
} occgrid;

// Connection
int BZRCConnect(char* address, int port, int* tcpSocket, FILE** fSocket);
int BZRCDisconnect(int tcpSocket, FILE* fSocket);

// Actions
int shoot(FILE* fSocket, int entID);
int setSpeed(FILE* fSocket, int entID, float value);
int setAngVel(FILE* fSocket, int entID, float value);
int setAccel(FILE* fSocket, int entID, int axis, float value); // Unsupported by the server

// Queries
int getTeams(FILE* fSocket, team** teams, int* teamCount);
int getObstacles(FILE* fSocket, obstacle** obstacles, int* obstacleCount);
int getBases(FILE* fSocket, base** bases, int* baseCount);
int getFlags(FILE* fSocket, flag** flags, int* flagCount);
int getShots(FILE* fSocket, shot** shots, int* shotCount);
int getMyTanks(FILE* fSocket, tank** tanks, int* tankCount);
int getOtherTanks(FILE* fSocket, enemyTank** tanks, int* tankCount);
int getConstants(FILE* fSocket, constant** constants, int* constantCount);
int getOccGrid(FILE* fSocket, occgrid* grid, int entID);

#endif

