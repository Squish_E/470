#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/freeglut.h>
#include "bzrc.h"
#include "gf_agent.h"

#define IP_ADDRESS "127.0.0.1"
#define WORLD_UPDATE_RATE 100000
#define SIXTY_DEG M_PI/3
#define TOMICRO(x) x*1000000

#define ALPHA 0.7f
#define BETA  0.65f
#define TAN_BETA 0.001f

#define MY_INFINITY 50

#define GOAL_INFLUENCE 40.0f
#define OBJ_INFLUENCE 60.0f
#define TAN_INFLUENCE 0.1f

extern int debug;

sem_t world_init_mutex;
sem_t update_mutex;

int running = 1;

float* grid;
int width;
int height;
float truePositive;
float trueNegative;
int subdivSize = 80;


int main(int argc, char* argv[]) {
	int tcpSocket, i = 0;
	world_info world;
	pthread_t* agents;
	pthread_t update_thread;
	pthread_t visualizer_thread;
	param* params;
	constant* constants;
	int constantCount;
	char* teamColor;

	// Initialize
	sem_init(&world_init_mutex, 0, 0);
	sem_init(&update_mutex, 0, 1);
	srand(time(NULL));	

	if (argc < 2) {
		fprintf(stderr, "usage: bzrc port [debug 1:0]\n");
		return -1;
	}
	if (argc == 3 && atoi(argv[2]) == 1) {
		debug = 1;
	}
	
	BZRCConnect(IP_ADDRESS, atoi(argv[1]), &tcpSocket, &world.fSocket);
	pthread_create(&update_thread, NULL, updater, (void*)&world);
	
	getConstants(world.fSocket, &constants, &constantCount);
	for (i = 0; i < constantCount; i++) {
		if (!strncmp("team", constants[i].name, 4)) {
			teamColor = constants[i].value;
		}
		else if (strcmp("worldsize", constants[i].name) == 0) {
			height = atoi(constants[i].value);
			width = atoi(constants[i].value);
			printf("World size: %d, %d\n", height, width);
		}
		else if (strcmp("truepositive", constants[i].name) == 0) {
			truePositive = atof(constants[i].value);
			printf("True Positive: %f\n", truePositive);
		}
		else if (strcmp("truenegative", constants[i].name) == 0) {
			trueNegative = atof(constants[i].value);
			printf("True Negative: %f\n", trueNegative);
		}
	}

	sem_wait(&world_init_mutex);
	pthread_create(&visualizer_thread, NULL, visualizer, (void*)&world);
	

	agents = (pthread_t*)malloc(sizeof(pthread_t) * world.tankCount);
	params = (param*)malloc(sizeof(params) * world.tankCount);
	for(i = 0; i < world.tankCount; i++) {
		params[i].tankID = world.tanks[i].index;
		params[i].teamColor = teamColor;
		params[i].world = &world;
		pthread_create(&agents[i], NULL, runAgent, (void*)&params[i]);
	}
	for(i = 0; i < world.tankCount; i++) {
		pthread_join(agents[i], NULL);
	}
	pthread_join(visualizer_thread, NULL);
	pthread_join(update_thread, NULL);
	free(params);
	free(agents);
	free(constants);
	free(world.flags);
	free(world.tanks);
	free(world.bases);
	free(world.obstacles);
	sem_destroy(&world_init_mutex);
	sem_destroy(&update_mutex);
	BZRCDisconnect(tcpSocket, world.fSocket);

	return 0;
}

void drawGrid() {
	int draw = 0;
        glRasterPos2f(-1, -1);
        glDrawPixels(width, height, GL_LUMINANCE, GL_FLOAT, grid);
        glFlush();
        glutSwapBuffers();
        glutPostRedisplay();
        draw++;
        if (draw > 50) glutLeaveMainLoop();
        if (!running) glutLeaveMainLoop();
}

void *visualizer(void* args) {
	FILE* fSocket;
	world_info* world = (world_info*) args;
	fSocket = world->fSocket;
	int i,j;
	int argc = 0;
	char** argv = 0;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);
        glutInitWindowSize(width, height);
        glutInitWindowPosition(0, 0);
        glutCreateWindow("Grid Filter");
        grid = (float*)malloc(sizeof(float) * width * height);
        for (i = 0; i < height; i++) {
                for (j = 0; j < width; j++) {
                        grid[i*width+j] = 0.75f;
                        //grid[i*width+j] = 0.0f;
                }
        }
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
        glutDisplayFunc(drawGrid);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        //signal(SIGINT, intHandler); // have to re-register these handlers since glut overwrote them
        //signal(SIGKILL, intHandler);
        glutMainLoop();
}

void *updater(void* args) {
	FILE* fSocket;
	world_info* world = (world_info*)args;
	fSocket = world->fSocket;
	getMyTanks(fSocket, &world->tanks, &world->tankCount);
	sem_post(&world_init_mutex);
	while (running) {
		usleep(WORLD_UPDATE_RATE);
		sem_wait(&update_mutex);
			free(world->tanks);
			getMyTanks(fSocket, &world->tanks, &world->tankCount);
		sem_post(&update_mutex);
	}
	return 0;
}

float normalizeAngle(float angle) {
	angle -= 2 * M_PI * (int)(angle/(2*M_PI));
	if (angle <= -M_PI) {
		angle += 2 * M_PI;
	}
	else if (angle > M_PI) {
		angle -= 2 * M_PI;
	}
	return angle;
}
int getGoal(int* retID, pair* retGoal, param* agent, tank curTank, int goal) {
	int step = fmin(800/agent->world->tankCount, 50);
	int start = step * curTank.index + 50;
	if(goal == 0) {
		retGoal->x = start - (width/2);
		retGoal->y = (height/2) - 50;
	}
	else if(goal == 1) {
		retGoal->x = start - (width/2);
		retGoal->y = 50 - (height/2);
	}
	else if(goal % 5) {
		srand(time(NULL));
		retGoal->x = (rand() % width) - (width/2);
		retGoal->y = (rand() % height) - (height/2);
	}
	else {
		int i;
		int j;
		for(i = 50; i < height - 50; i++) {
			for(j = 50; j < width - 50; j++) {
				if(!(grid[j*width+i] < 0.1f || grid[j*width+i] > 0.9f)) {
					retGoal->x = i - (width/2);
					retGoal->y = j - (height/2);
					return;
				}
			}
		}
	}
}

int timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1) {
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;

    return (diff<0);
}

pair centerB(base obj) {
	pair toRet;
	toRet.x = (obj.corners[0].x + obj.corners[1].x + obj.corners[2].x + obj.corners[3].x) / 4;
	toRet.y = (obj.corners[0].y + obj.corners[1].y + obj.corners[2].y + obj.corners[3].y) / 4;
	return toRet;
}

pair center(obstacle obj) {
	pair toRet;
	toRet.x = (obj.corners[0].x + obj.corners[1].x + obj.corners[2].x + obj.corners[3].x) / 4;
	toRet.y = (obj.corners[0].y + obj.corners[1].y + obj.corners[2].y + obj.corners[3].y) / 4;
	return toRet;
}

double square(double toSquare) {
	return toSquare * toSquare;
}

double getDis(double one, double two) {
	return sqrt(square(one) + square(two));
}

double getDisP(pair one, pair two) {
	return getDis(one.x - two.x, two.y - one.y);
}

double getAngle(double one, double two) {
	return atan2(one, two);
}

double getAngleP(pair pointOne, pair pointTwo) {
	return getAngle(pointOne.y - pointTwo.y, pointOne.x - pointTwo.x);
}

pair attractField(pair cur_pos, pair goal_pos, double radius) {
	double dist = getDisP(goal_pos, cur_pos);
	double angle = getAngleP(goal_pos, cur_pos);
	pair delta;
	if(dist < radius) {
		delta.x = 0;
		delta.y = 0;
	}
	else if(radius <= dist && dist <= (GOAL_INFLUENCE + radius)) {
		delta.x = ALPHA * (dist - radius) * cos(angle);
		delta.y = ALPHA * (dist - radius) * sin(angle);
	}
	else if (dist > (radius + GOAL_INFLUENCE)) {
		delta.x = ALPHA * GOAL_INFLUENCE * cos(angle);
		delta.y = ALPHA * GOAL_INFLUENCE * sin(angle);
	}
	return delta;
	
}

pair reflectField(pair cur_pos, pair obj_pos, double radius) {
	double dist = getDisP(obj_pos, cur_pos);
	double angle = getAngleP(obj_pos, cur_pos);
	pair delta;
	if (dist < radius) {
		delta.x = -cos(angle) * MY_INFINITY;
		delta.y = -sin(angle) * MY_INFINITY;
	}
	else if (radius <= dist && dist <= (OBJ_INFLUENCE + radius)) {
		delta.x = -BETA * (OBJ_INFLUENCE + radius - dist) * cos(angle);
		delta.y = -BETA * (OBJ_INFLUENCE + radius - dist) * sin(angle);
	}
	else if (dist > (OBJ_INFLUENCE + radius)) {
		delta.x = 0;
		delta.y = 0;
	}
	return delta;
}

pair tanField(pair cur_pos, pair obj_pos, double radius) {
	double dist = getDisP(obj_pos, cur_pos);
	double angle = getAngleP(obj_pos, cur_pos) + M_PI/2;
	pair delta;
	if (dist < radius) {
		delta.x = -cos(angle) * MY_INFINITY;
		delta.y = -sin(angle) * MY_INFINITY;
	}
	else if (radius <= dist && dist <= (OBJ_INFLUENCE + radius)) {
		delta.x = -TAN_BETA * (TAN_INFLUENCE + radius - dist) * cos(angle);
		delta.y = -TAN_BETA * (TAN_INFLUENCE + radius - dist) * sin(angle);
	}
	else if (dist > (TAN_INFLUENCE + radius)) {
		delta.x = 0;
		delta.y = 0;
	}
	return delta;
}

double moveAngle(double dx, double dy, double cur_angle, double prev_angle, float timeDiff) {
	double goal_angle, error_angle, true_angle;
	goal_angle = atan2(dy, dx);
	cur_angle = normalizeAngle(cur_angle);
	error_angle = goal_angle - cur_angle;
	true_angle = error_angle + ((error_angle - prev_angle) / timeDiff);
	if (true_angle > M_PI) {
		true_angle = true_angle - 2 * M_PI;
	}
	else if (true_angle < -M_PI) {
		true_angle = 2 * M_PI + true_angle;
	}
	return true_angle;
}

void updateGrid(occgrid sensor) {
	int startx, starty, endx, endy;
	int i, j;
	float bel_occ;
	float bel_unocc;

	startx = sensor.topleft.x + (height / 2);
	starty = sensor.topleft.y + (width / 2);
	endx = startx + sensor.size.x;
	endy = starty + sensor.size.y;

	for (i = startx; i < endx; i++) {
		for (j = starty; j < endy; j++) {
			if (sensor.grid[(i-startx)*(int)sensor.size.y+(j-starty)] == '1') {
				bel_occ = truePositive * grid[j * width + i];
				bel_unocc = (1.0f - trueNegative) * (1.0f - grid[j * width + i]);
			}
			else {
				bel_occ = (1.0f - truePositive) * grid[j * width + i];
				bel_unocc = trueNegative * (1.0f - grid[j * width + i]);
			}
			grid[j*width+i] = bel_occ / (bel_occ + bel_unocc);
                        if (grid[j*width+i] > 1.0f) grid[j*width+i] = 1.0f;
                        if (grid[j*width+i] < 0.0f) grid[j*width+i] = 0.0f;
		}
	}
}


void *runAgent(void *agent_param) {
	param *agent = (param*) agent_param;
	tank curTank;
	pair goal;
	int i, j;
	// potential fields
	pair cur_delta;
	obstacle curObj;
	double r, dx, dy, speed;
	// PD controller
	double prev_angle = 0;
	agent->flagGoalID = -1;
	struct timeval tvBegin, tvEnd, tvDiff;

	// Grid Filter
	occgrid sensor;

	int next_goal = 0;
	double prev_distance;
	double cur_distance;
	srand(time(NULL));
	while(running) {
		gettimeofday(&tvBegin, NULL);
		usleep(WORLD_UPDATE_RATE); //wait for a tick
		sem_wait(&update_mutex);
			curTank = agent->world->tanks[agent->tankID];
			cur_distance = fabs(getDisP(curTank.location, goal));
			if(cur_distance < 5 || next_goal == 0 || (fabs(cur_distance - prev_distance) < 0.5) && speed > 0.3) {
				getGoal(&agent->flagGoalID, &goal, agent, curTank, next_goal);
				next_goal++;
			}
			prev_distance = cur_distance;
			getOccGrid(agent->world->fSocket, &sensor, curTank.index);
		sem_post(&update_mutex);
		
		gettimeofday(&tvEnd, NULL);
		timeval_subtract(&tvDiff, &tvEnd, &tvBegin);

		// Grid Angle
		updateGrid(sensor);
		free(sensor.grid);

		dx = dy = 0;
		// Attractive Field toward Goal
		cur_delta = attractField(curTank.location, goal, 3.0f);
		dx += cur_delta.x;
		dy += cur_delta.y;
		// PD Controller
		prev_angle = moveAngle(dx, dy, curTank.angle, prev_angle, (float) tvDiff.tv_usec);
		speed = 1.2f * (-pow((fabs(prev_angle / M_PI)), (float)1.0f/4.0f) +1);
		// If we aren't aimed in the right direction don't move
		if(fabs(prev_angle) > M_PI - (M_PI/4)) {
			speed = -speed;
		} 
		else if(fabs(prev_angle) > M_PI / 4) {
			speed = 0;
		}
		//printf("Speed: %lf\n", speed);	
		setSpeed(agent->world->fSocket, agent->tankID, speed);
		setAngVel(agent->world->fSocket, agent->tankID, prev_angle / M_PI);
	}
}
