CC = gcc
CFLAGS = -g -pthread -I. -O3
LIBS = -lm -lglut -lGL -lGLU -lgsl -lgslcblas
SRC = bzrc.c
LAB = $(SRC) agent.c
GRID = $(SRC) gf_agent.c
KALMAN = $(SRC) kf_agent.c
DUMB = $(SRC) dumb.c
KALMAN_DUMB = $(SRC) kalman_dumb.c
TEST = $(SRC) bzrc_test.c
OBJ = $(SRC:.c=.o)

default:
	$(CC) $(CFLAGS) $(LAB) -o lab1.o $(LIBS) 
grid:
	$(CC) $(CFLAGS) $(GRID) -o lab2.o $(LIBS) 
kf:
	$(CC) $(CFLAGS) $(KALMAN) -o lab3.o $(LIBS)
kfdumb:
	$(CC) $(CFLAGS) $(KALMAN_DUMB) -o kf_dumb.o
dumb:
	$(CC) $(CFLAGS) $(DUMB) -o dumb.o
test:
	$(CC) $(CFLAGS) $(TEST) -o test.o

clean:
	rm -f *.o
