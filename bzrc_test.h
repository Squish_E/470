#ifndef BZRC_TEST_H
#define BZRC_TEST_H

// Test Routines
void printTeams(FILE* fSocket);
void printObstacles(FILE* fSocket);
void printBases(FILE* fSocket);
void printFlags(FILE* fSocket);
void printShots(FILE* fSocket);
void printMyTanks(FILE* fSocket);
void printOtherTanks(FILE* fSocket);
void printConstants(FILE* fSocket);
void printOccGrid(FILE* fSocket, int entID);

#endif

