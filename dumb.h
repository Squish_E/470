#ifndef DUMB_H
#define DUMB_H

typedef struct agent_arg {
	int tankID;
	FILE* fSocket;
} param;

// Test Routines
void *dumbShoot(void *agent_param);
void *runAgent(void *agent_param);
float normalizeAngle(float angle);

#endif

