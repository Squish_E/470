#ifndef AGENT_H
#define AGENT_H
#include "bzrc.h"
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/freeglut.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>

#define WORLD_UPDATE_RATE 1000

typedef struct {
	FILE* fSocket;
	flag* flags;
	int flagCount;
	tank* tanks;
	int tankCount;
	base* bases;
	int baseCount;
        obstacle* obstacles;
        int obstacleCount;
} world_info;

typedef struct {
	int tankID;
	char* teamColor;
	world_info* world;
        int flagGoalID;
} param;

// Test Routines
void *runAgent(void* agent_param);
void *updater(void* args);
float normalizeAngle(float angle);
pair center(obstacle obj);
pair centerB(base b);
double square(double toSquare);
double getDis(double one, double two);
double getDisP(pair one, pair two);
double getAngle(double one, double two);
double getAngleP(pair pointOne, pair pointTwo);
void *runAgent(void *agent_param);
void plot(char* filename, gsl_matrix* u, gsl_matrix* sigma);

// KF Matricies
double F[36] =
{
	1,	0,	0,	0,	0,	0,
	0,	1,	0,	0,	0,	0,
	0,	0,	1,	0,	0,	0,
	0,	0,	0,	1,	0,	0,
	0,	0,	0,	0,	1,	0,
	0,	0,	0,	0,	0,	1
};

double H[12] =
{
	1,	0,	0,	0,	0,	0,
	0,	0,	0,	1,	0,	0
};

double H_TRANS[12] =
{
	1,	0,
	0,	0,
	0,	0,
	0,	1,
	0,	0,
	0,	0
};

double SIGMA_0[36] =
{
	100,	0,	0,	0,	0,	0,
	0,	0.1,	0,	0,	0,	0,
	0,	0,	0.1,	0,	0,	0,
	0,	0,	0,	100,	0,	0,
	0,	0,	0,	0,	0.1,	0,
	0,	0,	0,	0,	0,	0.1
};

double SIGMA_X[36] =
{
	0.01,	0,	0,	0,	0,	0,
	0,	0.01,	0,	0,	0,	0,
	0,	0,	100,	0,	0,	0,
	0,	0,	0,	0.01,	0,	0,
	0,	0,	0,	0,	0.01,	0,
	0,	0,	0,	0,	0,	100
};

double SIGMA_Z[4] =
{
	25,	0,
	0,	25
};
#endif
