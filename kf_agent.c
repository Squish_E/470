#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "bzrc.h"
#include "kf_agent.h"

#define IP_ADDRESS "127.0.0.1"
#define SIXTY_DEG M_PI/3
#define TOMICRO(x) x*1000000

#define ALPHA 0.7f
#define BETA  0.65f
#define TAN_BETA 0.001f

#define MY_INFINITY 50

#define GOAL_INFLUENCE 40.0f
#define OBJ_INFLUENCE 60.0f
#define TAN_INFLUENCE 0.1f

extern int debug;

sem_t world_init_mutex;
sem_t update_mutex;

int running = 1;

void printMatrix(gsl_matrix* matrix) {
        int i;
        int j;
        int rows = matrix->size1;
        int cols = matrix->size2;
        for (i = 0; i < rows; i++) {
                for (j = 0; j < cols; j++) {
                        printf("%2.8lf ", matrix->data[i * matrix->tda + j]);
                }
                printf("\n");
        }
        printf("\n");
}

int main(int argc, char* argv[]) {
	int tcpSocket, i = 0;
	world_info world;
	pthread_t* agents;
	pthread_t update_thread;
	param* params;
	constant* constants;
	int constantCount;
	char* teamColor;

	// Initialize
	sem_init(&world_init_mutex, 0, 0);
	sem_init(&update_mutex, 0, 1);
	srand(time(NULL));	

	if (argc < 2) {
		fprintf(stderr, "usage: bzrc port [debug 1:0]\n");
		return -1;
	}
	if (argc == 3 && atoi(argv[2]) == 1) {
		debug = 1;
	}
	
	BZRCConnect(IP_ADDRESS, atoi(argv[1]), &tcpSocket, &world.fSocket);
	pthread_create(&update_thread, NULL, &updater, (void*)&world);
	
	getConstants(world.fSocket, &constants, &constantCount);
	for(i = 0; i < constantCount; i++) {
		if(!strncmp("team", constants[i].name, 4)) {
			teamColor = constants[i].value;
			break;
		}
	}

	sem_wait(&world_init_mutex);
	//world.tankCount = 1;
	agents = (pthread_t*)malloc(sizeof(pthread_t) * world.tankCount);
	params = (param*)malloc(sizeof(params) * world.tankCount);
	for(i = 0; i < world.tankCount; i++) {
		params[i].tankID = world.tanks[i].index;
		params[i].teamColor = teamColor;
		params[i].world = &world;
		pthread_create(&agents[i], NULL, &runAgent, (void*)&params[i]);
	}
	for(i = 0; i < world.tankCount; i++) {
		pthread_join(agents[i], NULL);
	}
	free(params);
	free(agents);
	free(constants);
	free(world.flags);
	free(world.tanks);
	free(world.bases);
	free(world.obstacles);
	sem_destroy(&world_init_mutex);
	sem_destroy(&update_mutex);
	BZRCDisconnect(tcpSocket, world.fSocket);

	return 0;
}

void *updater(void* args) {
	FILE* fSocket;
	world_info* world = (world_info*)args;
	fSocket = world->fSocket;
	getFlags(fSocket, &world->flags, &world->flagCount);
	getMyTanks(fSocket, &world->tanks, &world->tankCount);
	getBases(fSocket, &world->bases, &world->baseCount); // bases are static, don't update
	getObstacles(fSocket, &world->obstacles, &world->obstacleCount);
	sem_post(&world_init_mutex);
	while (running) {
		usleep(WORLD_UPDATE_RATE);
		sem_wait(&update_mutex);
			free(world->flags);
			getFlags(fSocket, &world->flags, &world->flagCount);
			free(world->tanks);
			getMyTanks(fSocket, &world->tanks, &world->tankCount);
		sem_post(&update_mutex);
	}
	return 0;
}

float normalizeAngle(float angle) {
	angle -= 2 * M_PI * (int)(angle/(2*M_PI));
	if (angle <= -M_PI) {
		angle += 2 * M_PI;
	}
	else if (angle > M_PI) {
		angle -= 2 * M_PI;
	}
	return angle;
}

void matrixMultiply(gsl_matrix* A, gsl_matrix* B, gsl_matrix* C) {
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
			1.0, A, B,
			0.0, C);
}

gsl_matrix* invertMatrix(gsl_matrix* matrix) {
        int s;
        int n = matrix->size1;
        gsl_matrix * inverse = gsl_matrix_alloc (n, n);
        gsl_permutation * perm = gsl_permutation_alloc (n);
        gsl_linalg_LU_decomp(matrix, perm, &s);
        gsl_linalg_LU_invert(matrix, perm, inverse);
        return inverse;
}

void changeFMatrix(gsl_matrix* f, double dt) {
	gsl_matrix_set(f, 0, 1, dt);
	gsl_matrix_set(f, 0, 2, dt * dt / 2.0f);
	gsl_matrix_set(f, 1, 2, dt);
	gsl_matrix_set(f, 3, 4, dt);
	gsl_matrix_set(f, 3, 5, dt * dt / 2.0f);
	gsl_matrix_set(f, 4, 5, dt);
	return;
}

void kalmanGain(gsl_matrix* k,
		gsl_matrix* f,
		gsl_matrix* sigma_t,
		gsl_matrix* sigma_x,
		gsl_matrix* sigma_z,
		gsl_matrix* h,
		gsl_matrix* hT) {
	//K_{t+1} = (F  \Sigma_t  F^T + \Sigma_X) H^T (H (F \Sigma_t F^T + \Sigma_X)H^T + \Sigma_Z)^-1
	gsl_matrix* fT		= gsl_matrix_alloc(6, 6);
	gsl_matrix* physics	= gsl_matrix_alloc(6, 6);
	gsl_matrix* physicsHelp = gsl_matrix_alloc(6, 6);
	gsl_matrix* first_half	= gsl_matrix_alloc(6, 2);
	gsl_matrix* fsecond_half= gsl_matrix_alloc(2, 6);
	gsl_matrix* second_half = gsl_matrix_alloc(2, 2);
	gsl_matrix* inverted;

	gsl_matrix_memcpy(fT, f);
	gsl_matrix_transpose(fT);

	matrixMultiply(f, sigma_t, physicsHelp);	// (F * \Sigma _t
	matrixMultiply(physicsHelp, fT, physics);	// * F^T
	gsl_matrix_add(physics, sigma_x);		// + \Sigma _x)
	matrixMultiply(physics, hT, first_half);	// * H^T

	matrixMultiply(h, physics, fsecond_half);	// (H * (F * \Sigma _t * F^T + \Sigma _x)
	matrixMultiply(fsecond_half, hT, second_half);	// * h^T
	gsl_matrix_add(second_half, sigma_z);		// + \Sigma _z)
	
	inverted = invertMatrix(second_half);	 	// ^-1
	matrixMultiply(first_half, inverted, k);	// first * second

	gsl_matrix_free(inverted);
	gsl_matrix_free(second_half);
	gsl_matrix_free(fsecond_half);
	gsl_matrix_free(first_half);
	gsl_matrix_free(physicsHelp);
	gsl_matrix_free(physics);
	gsl_matrix_free(fT);
}

void calcMean(gsl_matrix* ret,
		gsl_matrix* mu,
		gsl_matrix* f,
		gsl_matrix* k,
		gsl_matrix* z,
		gsl_matrix* h) {
	// \mu_{t+1} = F \mu_t + K_{t+1}(z_{t+1} - H F \mu_t)
	gsl_matrix* f_mu	= gsl_matrix_alloc(6, 1);
	gsl_matrix* h_f 	= gsl_matrix_alloc(2, 6);
	gsl_matrix* h_f_mu 	= gsl_matrix_alloc(2, 1);

	matrixMultiply(f, mu, f_mu);

	matrixMultiply(h, f, h_f);
	matrixMultiply(h_f, mu, h_f_mu);
	gsl_matrix_sub(z, h_f_mu);
	matrixMultiply(k, z, ret);

	gsl_matrix_add(ret, f_mu);

	gsl_matrix_free(h_f_mu);
	gsl_matrix_free(h_f);
	gsl_matrix_free(f_mu);
}

void calcVariance(gsl_matrix* f,
		  gsl_matrix* h,
		  gsl_matrix* sigma_t,
		  gsl_matrix* sigma_x,
		  gsl_matrix* k,
		  gsl_matrix* sigma_t1) {
	gsl_matrix* physics	= gsl_matrix_alloc(6, 6);
	gsl_matrix* physicsHelp = gsl_matrix_alloc(6, 6);
	gsl_matrix* i		= gsl_matrix_alloc(6, 6);
	gsl_matrix* k_h		= gsl_matrix_alloc(6, 6);
	gsl_matrix_set_identity(i);
	gsl_matrix* fT		= gsl_matrix_alloc(6, 6);

	gsl_matrix_memcpy(fT, f);
	gsl_matrix_transpose(fT);

	matrixMultiply(k, h, k_h);
	gsl_matrix_sub(i, k_h);

	matrixMultiply(f, sigma_t, physicsHelp);	// (F * \Sigma _t
	matrixMultiply(physicsHelp, fT, physics);	// * F^T
	gsl_matrix_add(physics, sigma_x);		// + \Sigma _x)
	
	matrixMultiply(i, physics, sigma_t1);
}

int timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1) {
    long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
    result->tv_sec = diff / 1000000;
    result->tv_usec = diff % 1000000;

    return (diff<0);
}

pair centerB(base obj) {
	pair toRet;
	toRet.x = (obj.corners[0].x + obj.corners[1].x + obj.corners[2].x + obj.corners[3].x) / 4;
	toRet.y = (obj.corners[0].y + obj.corners[1].y + obj.corners[2].y + obj.corners[3].y) / 4;
	return toRet;
}

pair center(obstacle obj) {
	pair toRet;
	toRet.x = (obj.corners[0].x + obj.corners[1].x + obj.corners[2].x + obj.corners[3].x) / 4;
	toRet.y = (obj.corners[0].y + obj.corners[1].y + obj.corners[2].y + obj.corners[3].y) / 4;
	return toRet;
}

double square(double toSquare) {
	return toSquare * toSquare;
}

double getDis(double one, double two) {
	return sqrt(square(one) + square(two));
}

double getDisP(pair one, pair two) {
	return getDis(one.x - two.x, two.y - one.y);
}

double getAngle(double one, double two) {
	return atan2(one, two);
}

double getAngleP(pair pointOne, pair pointTwo) {
	return getAngle(pointOne.y - pointTwo.y, pointOne.x - pointTwo.x);
}

pair attractField(pair cur_pos, pair goal_pos, double radius) {
	double dist = getDisP(goal_pos, cur_pos);
	double angle = getAngleP(goal_pos, cur_pos);
	pair delta;
	if(dist < radius) {
		delta.x = 0;
		delta.y = 0;
	}
	else if(radius <= dist && dist <= (GOAL_INFLUENCE + radius)) {
		delta.x = ALPHA * (dist - radius) * cos(angle);
		delta.y = ALPHA * (dist - radius) * sin(angle);
	}
	else if (dist > (radius + GOAL_INFLUENCE)) {
		delta.x = ALPHA * GOAL_INFLUENCE * cos(angle);
		delta.y = ALPHA * GOAL_INFLUENCE * sin(angle);
	}
	return delta;
	
}

double moveAngle(double goal_x, double goal_y, double cur_angle, double prev_angle, float timeDiff) {
	double goal_angle, error_angle, true_angle;
	goal_angle = atan2(goal_y, goal_x);
	cur_angle = normalizeAngle(cur_angle);
	error_angle = goal_angle - cur_angle;
	true_angle = error_angle + ((error_angle - prev_angle) / timeDiff);
	if (true_angle > M_PI) {
		true_angle = true_angle - 2 * M_PI;
	}
	else if (true_angle < -M_PI) {
		true_angle = 2 * M_PI + true_angle;
	}
	return true_angle;
}

void toGslMatrix(gsl_matrix* matrix, double* d_matrix) {
	int i,j;
	for(i = 0; i < matrix->size1; i++) {
		for(j = 0; j < matrix->size2; j++) {
			gsl_matrix_set(matrix, i, j, d_matrix[i * matrix->tda + j]);
		}
	}
}

void *runAgent(void *agent_param) {
	param *agent = (param*) agent_param;
	tank curTank;
	pair goal;
	int i, j;
	// Kalman Filter
	gsl_matrix* f		= gsl_matrix_calloc(6, 6);
	gsl_matrix* h		= gsl_matrix_calloc(2, 6);
	gsl_matrix* hT		= gsl_matrix_calloc(6, 2);
        gsl_matrix* k		= gsl_matrix_calloc(6, 2);
        gsl_matrix* mu		= gsl_matrix_calloc(6, 1);
        gsl_matrix* mut1	= gsl_matrix_calloc(6, 1);
        gsl_matrix* z		= gsl_matrix_calloc(2, 1);
        gsl_matrix* sigma_t	= gsl_matrix_calloc(6, 6);
        gsl_matrix* sigma_t1	= gsl_matrix_calloc(6, 6);
        gsl_matrix* sigma_z	= gsl_matrix_calloc(2, 2);
        gsl_matrix* sigma_x	= gsl_matrix_calloc(6, 6);
	gsl_matrix* future	= gsl_matrix_calloc(6, 1);
	// Initialize
	toGslMatrix(f, F);
	toGslMatrix(h, H);
	toGslMatrix(hT, H_TRANS);
	toGslMatrix(sigma_t, SIGMA_0);
	toGslMatrix(sigma_x, SIGMA_X);
	toGslMatrix(sigma_z, SIGMA_Z);
	// Visualizer
	char plotFileName[256];	
	// PD controller
	pair cur_delta;
	double prev_angle = 0;
	agent->flagGoalID = -1;
	struct timeval tvBegin, tvEnd, tvDiff;

	enemyTank* enemyTanks;
	int enemyTankCount;
	getOtherTanks(agent->world->fSocket, &enemyTanks, &enemyTankCount);
	setSpeed(agent->world->fSocket, agent->tankID, 0);

	while(running) {
		gettimeofday(&tvBegin, NULL);
		usleep(WORLD_UPDATE_RATE); //wait for a tick

		sem_wait(&update_mutex);

		curTank = agent->world->tanks[agent->tankID];
		free(enemyTanks);
		getOtherTanks(agent->world->fSocket, &enemyTanks, &enemyTankCount);

		sem_post(&update_mutex);

		gettimeofday(&tvEnd, NULL);
		timeval_subtract(&tvDiff, &tvEnd, &tvBegin);

		changeFMatrix(f, ((float)tvDiff.tv_usec) / 1000000.0f);
		if((double)enemyTanks[0].location.x < -400) continue;
		// Kalman Filter
		gsl_matrix_set(z, 0, 0, (double)enemyTanks[0].location.x);
		gsl_matrix_set(z, 1, 0, (double)enemyTanks[0].location.y);
		
		kalmanGain(k, f, sigma_t, sigma_x, sigma_z, h, hT);
		calcMean(mut1, mu, f, k, z, h);
		calcVariance(f, h, sigma_t, sigma_x, k, sigma_t1);

		gsl_matrix_memcpy(mu, mut1);
		gsl_matrix_memcpy(sigma_t, sigma_t1);

		//goal.x = gsl_matrix_get(mu, 0, 0);
		//goal.y = gsl_matrix_get(mu, 3, 0);
		//printf("Tank is      at: %lf, %lf\n", goal.x, goal.y);

		// Predict in the future based on how far away the tank is
		//printMatrix(f);
		changeFMatrix(f, gsl_matrix_get(f, 0, 1) * getDisP(curTank.location, goal)*2);
		//printMatrix(f);
		matrixMultiply(f, mu, future);

		goal.x = gsl_matrix_get(future, 0, 0);
		goal.y = gsl_matrix_get(future, 3, 0);
		if(goal.x > 400) goal.x = 400;
		if(goal.x < -400) goal.x = -400;
		if(goal.y > 400) goal.y = 400;
		if(goal.y < -400) goal.y = -400;
		//printf("Tank will be at: %lf, %lf\n", goal.x, goal.y);
		// PD Controller
		cur_delta = attractField(curTank.location, goal, 3.0f);
		//printf("curd: %lf %lf\ngoal: %lf %lf\n",cur_delta.x, cur_delta.y, goal.x, goal.y);
		prev_angle = moveAngle(cur_delta.x, cur_delta.y, curTank.angle, prev_angle, (float) tvDiff.tv_usec);
		if(fabs(prev_angle) < M_PI/16) {
			setAngVel(agent->world->fSocket, agent->tankID, 8*(prev_angle / M_PI));
			
			if(fabs(prev_angle) < M_PI/24) 
				shoot(agent->world->fSocket, agent->tankID);
		}
		else {
			setAngVel(agent->world->fSocket, agent->tankID, 4*(prev_angle / M_PI));
		}
		//printf("angle: %lf\n", prev_angle);

		sprintf(plotFileName, "plot%d.svg", i++);
                //plot(plotFileName, mu, sigma_t);
	}

	gsl_matrix_free(future);
	gsl_matrix_free(sigma_x);
	gsl_matrix_free(sigma_z);
	gsl_matrix_free(sigma_t1);
	gsl_matrix_free(sigma_t);
	gsl_matrix_free(z);
	gsl_matrix_free(mut1);
	gsl_matrix_free(mu);
	gsl_matrix_free(k);
	gsl_matrix_free(hT);
	gsl_matrix_free(h);
	gsl_matrix_free(f);
}

void plot(char* filename, gsl_matrix* u, gsl_matrix* sigma) {
        FILE* outputFile;
        char* gnuplotArgs[4];
        char plotDataFileName[256];
        char gnuplotLocation[] = "/usr/bin/gnuplot";
        char gnuplotFile[] = "myplot.gpi";
        char gnuplotArg[] = "-persist";
        sprintf(plotDataFileName, "%s.gpi", filename);
        outputFile = fopen(plotDataFileName, "w");
        fprintf(outputFile, "set xrange [-400.0: 400.0]\n");
        fprintf(outputFile, "set yrange [-400.0: 400.0]\n");
        fprintf(outputFile, "set pm3d\n");
        fprintf(outputFile, "set view map\n");
        
        fprintf(outputFile, "set term svg\n");
        fprintf(outputFile, "set output '%s'\n", filename);

        fprintf(outputFile, "unset key\n");
        fprintf(outputFile, "set size square\n");
        fprintf(outputFile, "set palette model RGB functions 1-gray, 1-gray, 1-gray\n");
        fprintf(outputFile, "set isosamples 100\n");
        fprintf(outputFile, "sigma_x = %f\n", gsl_matrix_get(sigma, 0, 0));
        fprintf(outputFile, "sigma_y = %f\n", gsl_matrix_get(sigma, 3, 3));
        fprintf(outputFile, "mean_x = %f\n", gsl_matrix_get(u, 0, 0));
        fprintf(outputFile, "mean_y = %f\n", gsl_matrix_get(u, 3, 0));
        fprintf(outputFile, "splot (1.0/(2.0 * pi * sigma_x * sigma_y) * exp(-1.0/2.0 * ((x - mean_x)**2 / sigma_x**2 + (y - mean_y)**2 / sigma_y**2))) with pm3d\n");
        fclose(outputFile);
        if (fork() == 0) {
                gnuplotArgs[0] = gnuplotLocation;
                gnuplotArgs[1] = plotDataFileName;
                gnuplotArgs[2] = gnuplotArg;
                gnuplotArgs[3] = 0;
                execv(gnuplotLocation, gnuplotArgs); 
        }
        return;
}
