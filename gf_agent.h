#ifndef AGENT_H
#define AGENT_H
#include "bzrc.h"
typedef struct {
	FILE* fSocket;
	flag* flags;
	int flagCount;
	tank* tanks;
	int tankCount;
	base* bases;
	int baseCount;
        obstacle* obstacles;
        int obstacleCount;
} world_info;

typedef struct {
	int tankID;
	char* teamColor;
	world_info* world;
        int flagGoalID;
} param;

void *runAgent(void* agent_param);
void *updater(void* args);
void *visualizer(void* args);
float normalizeAngle(float angle);
int getGoal(int* retID, pair* retGoal, param* agent, tank curTanki, int goal);
pair center(obstacle obj);
pair centerB(base b);
double square(double toSquare);
double getDis(double one, double two);
double getDisP(pair one, pair two);
double getAngle(double one, double two);
double getAngleP(pair pointOne, pair pointTwo);

#endif

